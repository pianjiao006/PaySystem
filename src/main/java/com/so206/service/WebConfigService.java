package com.so206.service;

import com.so206.po.SystemWebWithBLOBs;

public interface WebConfigService {

    SystemWebWithBLOBs find_by_id(Integer id);

    void update_web_config(SystemWebWithBLOBs webconfig);

}
