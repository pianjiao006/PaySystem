create table sys_extension(
  id int primary key auto_increment,
  e_id int comment '推广人id',
  e_name varchar(128) comment '推广用户名',
  r_id int comment '注册人id',
  r_name varchar(128) comment '注册人用户名',
  status int comment '状态:0.审核中,1.审核通过,2.审核失败',
  money decimal(10,2) comment '本次推广获得佣金',
  create_time datetime comment '创建时间',
  e_key varchar(128) comment '验证密文'
)engine innodb charset utf8;
alter table system_web add exten_status int  default 0 comment '是否开启推广:0.不开启,1.开启';
alter table system_web add exten_min_money decimal(10,2) default 0.01 comment '推广获得最小金额';
alter table system_web add exten_max_money decimal(10,2) default 1.00 comment '推广获得最大金额';
alter table system_order add exten_key varchar(256) default null comment '验证密文';
alter table system_web add order_filter int default 1 comment '商品过滤:0.off,q.on';
INSER TINTO sys_function (id, fun_name, fun_descp, fun_path, parent, status, opt_time, is_menu) VALUES (28, '返利管理', 'sys_extension_list', '/admin/extension/list', 8, 0, '2018-06-26 14:32:07', 1);
INSER TINTO sys_function (id, fun_name, fun_descp, fun_path, parent, status, opt_time, is_menu) VALUES (29, '返利详情', 'user_extension_list', '/admin/extension/list_user', 0, 0, '2018-05-16 17:51:41', 1);
